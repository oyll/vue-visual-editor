import {defineComponent, PropType, reactive, computed, getCurrentInstance, createApp, ComponentPublicInstance, ref, onBeforeUnmount, provide, inject} from 'vue'
import './dropdown-service.scss'

interface DropdownServiceOption {
    reference: [number, number] | MouseEvent | HTMLElement,
    render: () => any,
}

interface DropdownInstance {
    service: (option: DropdownServiceOption) => void,
    state: {
        option: DropdownServiceOption,
        showFlag: boolean,
        openFlag: boolean,
    },
    handler: {
        onClickOption: (e: MouseEvent) => void
    },
}

const DropdownProvider = (() => {
    const DROPDOWN_PROVIDER = '@@DROPDOWN_PROOVIDER'
    return {
        provide: (data: DropdownInstance) => provide(DROPDOWN_PROVIDER, data),
        inject: () => inject(DROPDOWN_PROVIDER) as DropdownInstance
    }
})();

const Component = defineComponent({
    props: {
        option: {type: Object as PropType<DropdownServiceOption>, required: true},
    },
    setup(props) {

        const el = ref(null as any as HTMLDivElement)
        const ctx = getCurrentInstance()!
        const state = reactive({
            option: props.option,
            showFlag: false,
            openFlag: false,
        })
        const styles = computed(() => {
            let x = 0, y = 0;
            const reference = state.option.reference
            if (Array.isArray(reference)) {
                x = reference[0]
                y = reference[1]
            } else if ('initMouseEvent' in reference) {
                x = reference.clientX - 20
                y = reference.clientY - 20
            } else {
                const {top, left, height} = reference.getBoundingClientRect()
                x = left
                y = top + height
            }
            return {
                left: `${x}px`,
                top: `${y}px`,
            }
        })
        const methods = {
            service: (option: DropdownServiceOption) => {
                state.option = option
                state.showFlag = true
            }
        }
        const handler = {
            onClickWindow: (e: MouseEvent) => {
                if (el.value.contains(e.target as Node)) {
                    /*点击了dropdown content*/
                    return
                } else {
                    state.showFlag = false
                }
            },
            onClickOption: () => state.showFlag = false,
        }
        const refer = {
            handler,
            ...methods,
            state,
        }
        Object.assign(ctx.proxy, refer)
        DropdownProvider.provide(refer)

        document.body.addEventListener('mouseup', handler.onClickWindow, true)
        onBeforeUnmount(() => document.body.addEventListener('mouseup', handler.onClickWindow, true))

        return () => (
            <div
                ref={el}
                class={['dropdown-service', {'dropdown-service-show': state.showFlag}]}
                style={styles.value}>
                <div class="dropdown-service-content">
                    {state.option.render()}
                </div>
            </div>
        )
    },
})

export const DropdownOption = defineComponent({
    name: 'dropdown-option',
    props: {
        icon: {type: String},
        label: {type: String},
    },
    emits: {
        click: (e: MouseEvent) => true,
    },
    setup(props) {
        const ctx = getCurrentInstance()!
        const dropdown = DropdownProvider.inject()
        const onClick = (e: MouseEvent) => {
            e.stopPropagation()
            ctx.emit('click', e)
            dropdown.handler.onClickOption(e)
        }
        return () => (
            <div class="dropdown-option" onClick={onClick}>
                {!!props.icon && <i class={`iconfont ${props.icon}`}/>}
                {!!props.label && <span>{props.label}</span>}
                {ctx.slots.default && ctx.slots.default()}
            </div>
        )
    },
})

export const $$dropdown = (() => {

    function newInstance(option: DropdownServiceOption) {
        const app = createApp(Component, {option})
        const el = document.createElement('div')
        document.body.appendChild(el)
        return app.mount(el) as ComponentPublicInstance & DropdownInstance
    }

    let insList: (ComponentPublicInstance & DropdownInstance)[] = []

    return (option: DropdownServiceOption) => {
        let available = insList.find(ins => !ins.state.showFlag && !ins.state.openFlag)
        if (!available) {
            available = newInstance(option)
            insList.push(available)
        }
        available.service(option)
    }
})();