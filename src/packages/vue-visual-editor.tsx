import {computed, defineComponent, getCurrentInstance, PropType, reactive, ref, withModifiers, provide} from "vue";
import './editor.scss'
import {VisualEditorBlock, VisualEditorModelValue, VisualEditorComponent, VisualEditorOption} from "@/packages/editor.utils";
import {useModel} from "@/packages/utils/useModel";
import {VisualEditorBlockRender} from "@/packages/vue-visual-editor-block";
import {$dialog} from "@/packages/utils/dialog-service";
import {ElNotification} from 'element-plus'
import {useVisualCommander} from "@/packages/editor.command";
import {usePlainEvent} from "@/packages/plugins/event";
import {$$dropdown, DropdownOption} from "@/packages/utils/dropdown-service";
import {VueVisualEditorOperator, VueVisualEditorOperatorInstance} from "@/packages/vue-visual-editor-operator";
import {BlockResizeProvider} from "@/packages/components/block-resize/block-resize";

export const VueVisualEditor = defineComponent({
    props: {
        modelValue: {type: Object as PropType<VisualEditorModelValue>, required: true},     // 容器数据
        option: {type: Object as PropType<VisualEditorOption>, required: true},             // 编辑器配置信息
        formData: {type: Object as PropType<Record<string, any>>, required: true},          // 绑定的表单对象
        preview: {type: Boolean},                                                           // 当前是否预览
        welt: {type: Number, default: 5},                                                   // 贴边距离
        customBlockProps: {type: Object as PropType<Record<string, (() => Record<string, any>) | Record<string, any>>>},    // 自定义block的属性，用来实现业务逻辑
    },
    emits: {
        'update:modelValue': (data?: VisualEditorModelValue) => true,
        'update:option': (val?: VisualEditorOption) => true,
        'update:preview': (flag?: boolean) => true,
        dragstart: (blocks: VisualEditorBlock[]) => true,
        dragend: (blocks: VisualEditorBlock[]) => true,
    },
    setup(props) {

        const ctx = getCurrentInstance()!
        const operatorRef = ref<VueVisualEditorOperatorInstance>()
        const state = reactive({
            selectBlock: null as null | VisualEditorBlock,
            // editFlag: true,
            editFlag: false,
        })
        const dataModel = useModel(() => props.modelValue, val => ctx.emit('update:modelValue', val))
        const optionModel = useModel(() => props.option, (val) => ctx.emit('update:option', val))
        const previewModel = useModel(() => props.preview, (val) => ctx.emit('update:preview', val))
        const containerRef = ref(null as any as HTMLDivElement)
        const bodyRef = ref(null as any as HTMLDivElement)
        const event = usePlainEvent({
            dragstart: () => {ctx.emit('dragstart', dataModel.value.blocks)},
            dragend: () => {ctx.emit('dragend', dataModel.value.blocks)},
        })
        BlockResizeProvider.provide(event)
        /*跟节点样式*/
        const classes = computed(() => [
            'vue-visual-editor',
            {
                'vue-visual-editor-preview': previewModel.value === true,
            }
        ])
        /*容器节点样式*/
        const containerStyles = computed(() => ({
            width: `${dataModel.value.container.width}px`,
            height: `${dataModel.value.container.height}px`,
        }))
        /*当前选中block数据*/
        const focusData = computed(() => {
            let focus: VisualEditorBlock[] = [];
            let notFocus: VisualEditorBlock[] = [];
            dataModel.value.blocks.forEach(item => (item.focus ? focus : notFocus).push(item))
            return {
                focus, notFocus,
                isMultipleSelected: focus.length > 1,
            }
        })
        /*菜单组件拖拽*/
        const componentDraggier = {
            dragComponent: null as null | VisualEditorComponent,
            onComponentItemDragStart: (component: VisualEditorComponent) => {
                componentDraggier.dragComponent = component
                containerRef.value.addEventListener('dragenter', componentDraggier.onDragEnterContainer)
                containerRef.value.addEventListener('dragover', componentDraggier.onDragOverContainer)
                containerRef.value.addEventListener('dragleave', componentDraggier.onDragLeaveContainer)
                containerRef.value.addEventListener('drop', componentDraggier.onDropContainer)
                event.emit.dragstart()
            },
            onComponentItemDragEnd: () => {
                componentDraggier.dragComponent = null
                containerRef.value.removeEventListener('dragenter', componentDraggier.onDragEnterContainer)
                containerRef.value.removeEventListener('dragover', componentDraggier.onDragOverContainer)
                containerRef.value.removeEventListener('dragleave', componentDraggier.onDragLeaveContainer)
                containerRef.value.removeEventListener('drop', componentDraggier.onDropContainer)
            },
            onDragEnterContainer: (e: DragEvent) => {
                e.dataTransfer!.dropEffect = 'move'
            },
            onDragOverContainer: (e: DragEvent) => {
                e.preventDefault()
            },
            onDragLeaveContainer: (e: DragEvent) => {
                e.dataTransfer!.dropEffect = 'none'
            },
            onDropContainer: (e: DragEvent) => {
                const data = [...dataModel.value.blocks]
                data.push({
                    componentKey: componentDraggier.dragComponent!.key!,
                    focus: false,
                    adjustPosition: true,
                    top: e.offsetY,
                    left: e.offsetX,
                    width: 0,
                    height: 0,
                    zIndex: 100,
                })
                methods.updateBlocks(data)
                event.emit.dragend()
            },
        }
        /*容器内的block拖拽*/
        const blockDraggier = {
            /*---------------------------------------block-------------------------------------------*/
            mark: reactive({
                x: null as null | number,
                y: null as null | number,
            }),
            blockDragData: {
                shiftKey: false,
                /*拖拽开始时body的scrollTop,moveScrollTop 为拖拽过程中body的scrollTop值*/
                startScrollTop: 0,
                moveScrollTop: 0,
                /*拖拽元素，拖拽开始时候的top和left值*/
                startLeft: 0,
                startTop: 0,
                /*计时器，放置用户点击的时候发生拖拽*/
                startTime: 0,
                /*拖拽开始时选中的元素，所有选中的元素都会移动*/
                focusList: [] as VisualEditorBlock[],
                /*所有选中元素拖拽开始时的位置信息*/
                startPositionList: [] as { left: number, top: number }[],
                /*拖拽开始时的clientX和clientY*/
                startX: 0,
                startY: 0,
                /*拖拽移动过程中的clientX和clientY*/
                moveX: 0,
                moveY: 0,
                /*left是给block定位判断用的，showLeft是给mark显示标线用的*/
                availableMarks: {
                    x: [] as { left: number, showLeft: number }[],
                    y: [] as { top: number, showTop: number }[],
                },
                /*标记变量，第一次move的时候才派发dragstart事件，而不是mousedown*/
                dragging: false,
            },
            onMousedownBlock: (e: MouseEvent, data: VisualEditorBlock) => {
                if (previewModel.value) {
                    return
                }
                const {focus, notFocus} = focusData.value
                blockDraggier.blockDragData = {
                    shiftKey: e.shiftKey,
                    startScrollTop: bodyRef.value.scrollTop,
                    moveScrollTop: bodyRef.value.scrollTop,
                    startLeft: data.left,
                    startTop: data.top,
                    startTime: Date.now(),
                    focusList: focus,
                    startPositionList: focus.map(meta => ({left: meta.left, top: meta.top})),
                    startX: e.clientX,
                    startY: e.clientY,
                    moveX: e.clientX,
                    moveY: e.clientY,
                    dragging: false,
                    availableMarks: (() => {
                        let x: { left: number, showLeft: number }[] = []
                        let y: { top: number, showTop: number }[] = []
                        const list: { top: number, left: number, width: number, height: number }[] = [
                            ...notFocus,
                            {
                                top: 0,
                                left: 0,
                                width: dataModel.value.container.width!,
                                height: dataModel.value.container.height!,
                            }
                        ]
                        list.forEach(meta => {
                            x.push({left: meta.left, showLeft: meta.left})                                                      // 左对左
                            x.push({left: meta.left + meta.width, showLeft: meta.left + meta.width})                            // 右对左
                            x.push({left: meta.left + meta.width / 2 - data.width / 2, showLeft: meta.left + meta.width / 2})   // 中对中
                            x.push({left: meta.left + meta.width - data.width, showLeft: meta.left + meta.width})               // 右对右
                            x.push({left: meta.left - data.width, showLeft: meta.left})                                         // 左对右

                            y.push({top: meta.top, showTop: meta.top})                                                          // 上对上
                            y.push({top: meta.top + meta.height, showTop: meta.top + meta.height})                              // 下对上
                            y.push({top: meta.top + meta.height / 2 - data.height / 2, showTop: meta.top + meta.height / 2})    // 中对中
                            y.push({top: meta.top + meta.height - data.height, showTop: meta.top + meta.height})                // 下对下
                            y.push({top: meta.top - data.height, showTop: meta.top})                                            // 上对下
                        })
                        return {
                            x, y
                        }
                    })(),
                }
                document.body.addEventListener('mousemove', blockDraggier.onMousemoveDocument, true)
                document.body.addEventListener('mouseup', blockDraggier.onMouseupDocument, true)
                bodyRef.value.addEventListener('scroll', blockDraggier.onBodyScroll)
            },
            onMouseupDocument: () => {
                blockDraggier.mark.x = null
                blockDraggier.mark.y = null
                blockDraggier.blockDragData.shiftKey = false
                document.body.removeEventListener('mousemove', blockDraggier.onMousemoveDocument, true)
                document.body.removeEventListener('mouseup', blockDraggier.onMouseupDocument, true)
                bodyRef.value.removeEventListener('scroll', blockDraggier.onBodyScroll);

                blockDraggier.blockDragData.dragging && event.emit.dragend();
            },
            onMousemoveDocument: (e: MouseEvent) => {
                blockDraggier.blockDragData.moveX = e.clientX
                blockDraggier.blockDragData.moveY = e.clientY
                blockDraggier.blockDragData.shiftKey = e.shiftKey
                blockDraggier.handleDragMove()
            },
            onBodyScroll: () => {
                blockDraggier.blockDragData.moveScrollTop = bodyRef.value.scrollTop
                blockDraggier.handleDragMove()
            },
            handleDragMove: () => {
                if (Date.now() - blockDraggier.blockDragData.startTime < 100) {
                    return
                }
                if (!blockDraggier.blockDragData.dragging) {
                    event.emit.dragstart()
                    blockDraggier.blockDragData.dragging = true
                }
                const currentScrollTop = bodyRef.value.scrollTop
                let {shiftKey, focusList, startPositionList, startX, startY, startLeft, startTop, startScrollTop, moveX, moveY} = blockDraggier.blockDragData
                moveY += currentScrollTop - startScrollTop

                if (shiftKey) {
                    const durX = Math.abs(moveX - startX)
                    const durY = Math.abs(moveY - startY)
                    durX > durY ? moveY = startY : moveX = startX
                }
                /*---------------------------------------贴边start-------------------------------------------*/

                const currentLeft = startLeft + moveX - startX
                const currentTop = startTop + moveY - startY

                const {x, y} = blockDraggier.blockDragData.availableMarks

                let newMarkX: null | number = null
                for (let i = 0; i < x.length; i++) {
                    const {left, showLeft} = x[i]
                    if (Math.abs(left - currentLeft) < props.welt) {
                        newMarkX = showLeft
                        moveX = left + startX - startLeft
                        break
                    }
                }
                if (newMarkX != null) {
                    blockDraggier.mark.x = newMarkX
                } else {
                    if (blockDraggier.mark.x != null) {
                        blockDraggier.mark.x = null
                    }
                }

                let newMarkY: null | number = null
                for (let i = 0; i < y.length; i++) {
                    const {top, showTop} = y[i];
                    if (Math.abs(top - currentTop) < props.welt) {
                        newMarkY = showTop
                        moveY = top + startY - startTop
                        break
                    }
                }
                if (newMarkY != null) {
                    blockDraggier.mark.y = newMarkY
                } else {
                    if (blockDraggier.mark.y != null) {
                        blockDraggier.mark.y = null
                    }
                }
                /*---------------------------------------贴边end-------------------------------------------*/

                const durX = moveX - startX
                const durY = moveY - startY
                focusList.map((meta, index) => {
                    const startPosition = startPositionList[index]
                    meta.left = startPosition.left + durX
                    meta.top = startPosition.top + durY
                })
            },
        }
        /*选中相关事件处理函数*/
        const focusHandler = {
            /**
             * 监听鼠标 摁下 block元素事件
             * — 预览状态下不做任何处理
             * - 如果是右键不做任何处理
             * - 如果没有嗯shift键，则选中该节点
             * - 处理拖拽block元素动作
             * @author  韦胜健
             * @date    2021/1/10 22:08
             */
            onMousedownBlock: (e: MouseEvent, data: VisualEditorBlock) => {
                if (previewModel.value) {
                    return
                }
                if (e.button === 2) {
                    /*右键不做任何处理*/
                    return;
                }
                state.selectBlock = data
                if (e.shiftKey) {
                    if (focusData.value.focus.length <= 1) {
                        data.focus = true
                    } else {
                        data.focus = !data.focus
                    }
                } else {
                    if (!data.focus) {
                        data.focus = true
                        methods.clearFocus(data)
                    }
                }
                blockDraggier.onMousedownBlock(e, data)
            },
            /**
             * 监听鼠标摁起 container容器事件
             * - 取消所有block元素的选中状态
             * @author  韦胜健
             * @date    2021/1/10 22:09
             */
            onMousedownContainer: (e: MouseEvent) => {
                state.selectBlock = null
                if (!e.shiftKey) {
                    methods.clearFocus()
                }
            }
        }
        /*其他处理动作*/
        const handler = {
            /**
             * block右击动作
             * @author  韦胜健
             * @date    2021/1/11 23:36
             */
            onBlockContextmenu: (e: MouseEvent, block: VisualEditorBlock) => {
                if (previewModel.value) {
                    return
                }
                e.preventDefault()
                block.focus = true
                $$dropdown({
                    reference: e,
                    render: () => <>
                        <DropdownOption label="置顶节点" icon="icon-place-top" {...{onClick: commander.placeTop}}/>
                        <DropdownOption label="置底节点" icon="icon-place-bottom" {...{onClick: commander.placeBottom}}/>
                        <DropdownOption label="删除节点" icon="icon-delete" {...{onClick: commander.delete}}/>
                        <DropdownOption label="查看数据" icon="icon-browse" {...{onClick: () => methods.showBlockData(block)}}/>
                        <DropdownOption label="导入节点" icon="icon-import" {...{onClick: () => methods.importBlockData(block)}}/>
                    </>
                })
            },
            /**
             * 操作栏更新容器配置信息
             * @author  韦胜健
             * @date    2021/1/11 23:37
             */
            onOperatorUpdateModelValue: (value: VisualEditorModelValue) => commander.updateModelValue(value),
            /**
             * 操作栏更新block配置信息
             * @author  韦胜健
             * @date    2021/1/11 23:37
             */
            onOperatorUpdateBlock: (newBlock: VisualEditorBlock, oldBlock: VisualEditorBlock) => methods.updateBlock(newBlock, oldBlock),
        }
        /*导出的工具函数*/
        const methods = {
            /**
             * 覆盖blocks数据
             * @author  韦胜健
             * @date    2021/1/11 12:36
             */
            updateBlocks: (blocks: VisualEditorBlock[]) => dataModel.value = {container: dataModel.value.container, blocks,},
            /**
             *更新单个block
             * @author  韦胜健
             * @date    2021/1/12 17:01
             */
            updateBlock: (newBlock: VisualEditorBlock, oldBlock: VisualEditorBlock) => {
                const blocks = [...dataModel.value.blocks]
                const index = blocks.indexOf(oldBlock)
                blocks.splice(index, 1, newBlock)
                commander.updateModelValue({...dataModel.value, blocks})
                state.selectBlock = dataModel.value.blocks[index]
            },
            /**
             * 取消选中所有的block
             * @author  韦胜健
             * @date    2021/1/11 12:36
             * @param   exclude            排除的block
             */
            clearFocus: (exclude?: VisualEditorBlock) => dataModel.value.blocks.forEach(d => (!exclude || exclude != d) && (d.focus = false)),
            /**
             * 删除当前选中的block
             * @author  韦胜健
             * @date    2021/1/11 12:36
             */
            delete: () => {
                const {notFocus} = focusData.value
                methods.updateBlocks(notFocus)
            },
            /**
             * 显示节点数据
             * @author  韦胜健
             * @date    2021/1/11 18:58
             */
            showBlockData: (block: VisualEditorBlock) => $dialog.textarea(JSON.stringify(block), {editReadonly: true}),
            /**
             * 导入节点数据
             * @author  韦胜健
             * @date    2021/1/11 18:58
             */
            importBlockData: async (block: VisualEditorBlock) => {
                const text = await $dialog.textarea()
                if (!text) {return}
                try {
                    const newBlock = JSON.parse(text)
                    const blocks = [...dataModel.value.blocks]
                    blocks.splice(dataModel.value.blocks.indexOf(block), 1, newBlock)
                    commander.updateModelValue({...dataModel.value, blocks,})
                } catch (e) {
                    console.error(e)
                    ElNotification({
                        message: '导入失败，数据格式不正确，请检查！',
                        type: 'error',
                    })
                }
            },
            openEdit: () => {
                state.editFlag = true
            }
        }

        /*命令管理对象*/
        const commander = useVisualCommander({dataModel, focusData, methods, event, apply: () => operatorRef.value!.apply()})
        /*操作栏按钮*/
        const buttons = [
            {label: '撤销', icon: 'icon-back', handler: commander.undo, tip: 'ctrl+z'},
            {label: '重做', icon: 'icon-forward', handler: commander.redo, tip: 'ctrl+y, ctrl+shift+z'},
            {
                label: () => previewModel.value ? '编辑' : '预览',
                icon: () => previewModel.value ? 'icon-edit' : 'icon-browse',
                handler: () => {
                    if (!previewModel.value) {methods.clearFocus()}
                    previewModel.value = !previewModel.value
                },
            },
            {
                label: '导入', icon: 'icon-import', handler: async () => {
                    const text = await $dialog.textarea('', {title: '请输入导入的JSON数据'})
                    if (!text) {return}
                    try {
                        const data = JSON.parse(text)
                        commander.updateModelValue(data)
                    } catch (e) {
                        ElNotification({
                            title: '导入失败！',
                            message: '导入的数据格式不正常，请检查！'
                        })
                    }
                }
            },
            {label: '导出', icon: 'icon-export', handler: () => $dialog.textarea(JSON.stringify(dataModel.value), {title: '导出的JSON数据', editReadonly: true})},
            {label: '置顶', icon: 'icon-place-top', handler: () => commander.placeTop(), tip: 'ctrl+up'},
            {label: '置底', icon: 'icon-place-bottom', handler: () => commander.placeBottom(), tip: 'ctrl+down'},
            {label: '删除', icon: 'icon-delete', handler: () => commander.delete(), tip: 'ctrl+d, backspace, delete'},
            {label: '清空', icon: 'icon-reset', handler: () => commander.clear(),},
            {
                label: '关闭', icon: 'icon-close', handler: () => {
                    methods.clearFocus()
                    state.editFlag = false
                },
            },
        ]

        return () => {
            return <>
                <div class="vue-visual-container"
                     style={containerStyles.value}>
                    {dataModel.value.blocks.map((d, index) => (
                        <VisualEditorBlockRender
                            key={index}
                            data={d}
                            option={optionModel.value}
                            formData={props.formData}
                            editSlots={ctx.slots}
                            preview={true}
                            customBlockProps={props.customBlockProps}
                        />))}
                    <div class="vue-visual-container-edit-button" onClick={methods.openEdit}>
                        <i class="iconfont icon-edit"/>
                        <span>编辑组件</span>
                    </div>
                </div>

                {state.editFlag && (<div class={classes.value}>
                    <div class="vue-visual-head">
                        <ul class="vue-visual-buttons">
                            {buttons.map(btn => {
                                const label = typeof btn.label === "function" ? btn.label() : btn.label
                                const icon = typeof btn.icon === "function" ? btn.icon() : btn.icon
                                return (
                                    <el-tooltip effect="dark" content={btn.tip} placement="bottom" disabled={!btn.tip}>
                                        <li key={label} onClick={() => !!btn.handler && btn.handler()}>
                                            <i class={`iconfont ${icon}`}/>
                                            <span>{label}</span>
                                        </li>
                                    </el-tooltip>
                                )
                            })}
                        </ul>
                    </div>
                    <div class="vue-visual-menu">
                        <div class="vue-visual-menu-head">
                            <i class="iconfont icon-entypomenu"/>
                            <span>组件列表</span>
                        </div>
                        <ul class="vue-visual-component-preview-list">
                            {optionModel.value.componentList.map((comp) => {
                                const Preview = comp.preview() as any
                                return (
                                    <li class="vue-visual-component-item"
                                        draggable
                                        onDragstart={() => componentDraggier.onComponentItemDragStart(comp)}
                                        onDragend={componentDraggier.onComponentItemDragEnd}
                                    >
                                        <Preview/>
                                        <div class="vue-visual-component-item-name">{comp.name}</div>
                                    </li>
                                )
                            })}
                        </ul>
                    </div>
                    <VueVisualEditorOperator
                        ref={operatorRef}
                        editorValue={dataModel.value}
                        selectBlocks={state.selectBlock}
                        option={props.option}
                        {...{
                            onUpdateModelValue: handler.onOperatorUpdateModelValue,
                            onUpdateBlock: handler.onOperatorUpdateBlock,
                        } as any}
                    />
                    <div class="vue-visual-body" ref={bodyRef}>
                        <div class="vue-visual-container"
                             style={containerStyles.value}
                             ref={containerRef}
                             {...{onMousedown: withModifiers(focusHandler.onMousedownContainer, ['self']),}}>
                            {
                                dataModel.value.blocks.map((d, index) => (<VisualEditorBlockRender
                                    key={index}
                                    data={d}
                                    option={optionModel.value}
                                    formData={props.formData}
                                    editSlots={ctx.slots}
                                    preview={previewModel.value}
                                    customBlockProps={props.customBlockProps}
                                    {...{
                                        onMousedown: (e: MouseEvent) => focusHandler.onMousedownBlock(e, d),
                                        onContextMenu: (e: MouseEvent) => handler.onBlockContextmenu(e, d),
                                    }}
                                />))
                            }
                            {blockDraggier.mark.x != null && <div class="vue-visual-x-mark" style={`left:${blockDraggier.mark.x}px`}/>}
                            {blockDraggier.mark.y != null && <div class="vue-visual-y-mark" style={`top:${blockDraggier.mark.y}px`}/>}
                        </div>
                    </div>
                </div>)}
            </>
        }
    },
})