import {defineComponent, onMounted, PropType, ref, computed} from 'vue';
import {VisualEditorBlock, VisualEditorOption} from "@/packages/editor.utils";
import {BlockResize} from "@/packages/components/block-resize/block-resize";

export const VisualEditorBlockRender = defineComponent({
    props: {
        data: {type: Object as PropType<VisualEditorBlock>, required: true},
        option: {type: Object as PropType<VisualEditorOption>, required: true},
        formData: {type: Object as PropType<Record<string, any>>, required: true},
        preview: {type: Boolean, required: true},
        editSlots: {type: Object, required: true},
        customBlockProps: {type: Object as PropType<Record<string, (() => Record<string, any>) | Record<string, any>>>},
    },
    setup(props) {

        const el = ref(null as any as HTMLDivElement)

        onMounted(() => {
            const {offsetWidth, offsetHeight} = el.value;
            const data = props.data as VisualEditorBlock;

            if (!data.width) data.width = offsetWidth
            if (!data.height) data.height = offsetHeight

            if (props.data.adjustPosition) {
                data.left = props.data.left - offsetWidth / 2;
                data.top = props.data.top - offsetHeight / 2;
                data.adjustPosition = false
            }
        })

        return () => {
            const {data, option} = props
            const renderProps = {
                block: {
                    style: {
                        top: `${data.top}px`,
                        left: `${data.left}px`,
                        zIndex: data.zIndex,
                    } as any,
                }
            }

            const Component = option.componentMap[data.componentKey]
            const renderData = {
                block: data,
                props: data.props || {},
                custom: (() => {
                    if (!(!!props.customBlockProps && !!data.slotName && !!props.customBlockProps[data.slotName])) {
                        return {}
                    }
                    const c = props.customBlockProps[data.slotName]
                    return typeof c === "function" ? c() : c
                })(),
                model: (() => {
                    const models = Object.entries(Component.model || {})
                    if (models.length === 0 || !data.model) return {}
                    const formData = props.formData as Record<string, any>

                    return models.reduce((prev, [modifier]) => {
                        const bindField = data.model![modifier]
                        prev[modifier] = {
                            [modifier]: formData[bindField],
                            [`onUpdate:${modifier}`]: (val: any) => formData[bindField] = val,
                        }
                        return prev
                    }, {} as Record<string, Record<string, any>>)
                })(),
            }
            let Render: any = null
            if (!!Component) {
                if (!!data.slotName && !!props.editSlots[data.slotName]) {
                    Render = props.editSlots[data.slotName](renderData)
                } else {
                    Render = Component.render(renderData)
                }
            }

            return (
                <div class={['vue-visual-block', {'vue-visual-block-focus': data.focus,}]}
                     {...renderProps.block}
                     ref={el}>
                    {!!Render && (Array.isArray(Render) ? Render : <Render/>)}
                    {!props.preview && !!Component.resize && data.focus && <BlockResize resize={Component.resize} block={data}/>}
                </div>
            )
        }
    },
})