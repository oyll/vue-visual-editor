import {defineComponent, inject, PropType, provide} from 'vue';
import {VisualEditorBlock} from "@/packages/editor.utils";
import deepcopy from 'deepcopy';

interface ResizeEvent {
    on: {
        dragstart: (cb: () => void) => void,
        dragend: (cb: () => void) => void,
    },
    off: {
        dragstart: (cb: () => void) => void,
        dragend: (cb: () => void) => void,
    },
    emit: {
        dragstart: () => void,
        dragend: () => void,
    },
}

export const BlockResizeProvider = (() => {
    const PROVIDER = '@@BlockResizeProvider'
    return {
        provide: (event: ResizeEvent) => provide(PROVIDER, event),
        inject: () => inject(PROVIDER) as ResizeEvent,
    }
})();

enum Direction {
    start = 'start',
    center = 'center',
    end = 'end',
}

export const BlockResize = defineComponent({
    props: {
        resize: {type: Object as PropType<{ width?: boolean, height?: boolean }>, required: true},
        block: {type: Object as PropType<VisualEditorBlock>, required: true},
    },
    setup(props) {
        const event = BlockResizeProvider.inject()
        const draggier = (() => {
            let data = {
                startBlock: props.block,
                startX: 0,
                startY: 0,
                direction: {vertical: Direction.start, horizontal: Direction.start},
                dragging: false,
            }

            const onMousedown = (e: MouseEvent, direction: { vertical: Direction, horizontal: Direction }) => {
                e.stopPropagation()
                data = {
                    startBlock: deepcopy(props.block),
                    startX: e.clientX,
                    startY: e.clientY,
                    direction,
                    dragging: false,
                }
                document.addEventListener('mousemove', onMousemove)
                document.addEventListener('mouseup', onMouseup)
            }
            const onMouseup = () => {
                document.removeEventListener('mousemove', onMousemove)
                document.removeEventListener('mouseup', onMouseup)
                if (data.dragging) {
                    event.emit.dragend()
                }
            }
            const onMousemove = (e: MouseEvent) => {
                const {clientX: moveX, clientY: moveY} = e
                const {startX, startY, direction, dragging, startBlock: {width, height, top, left}} = data
                const block = props.block as VisualEditorBlock
                if (!dragging) {
                    event.emit.dragstart()
                    data.dragging = true
                }
                let durX = direction.horizontal === Direction.center ? 0 : (moveX - startX)
                let durY = direction.vertical === Direction.center ? 0 : (moveY - startY)

                if (direction.horizontal === Direction.start) {
                    durX = -durX
                    block.left = left - durX
                }
                if (direction.vertical === Direction.start) {
                    durY = -durY
                    block.top = top - durY
                }

                block.width = width + durX
                block.height = height + durY
            }
            return {
                onMousedown,
            }
        })();

        return () => <>
            {!!props.resize.height && <>
                <div class="visual-block-resize  visual-block-resize-top" onMousedown={e => draggier.onMousedown(e, {vertical: Direction.start, horizontal: Direction.center})}/>
                <div class="visual-block-resize  visual-block-resize-bottom" onMousedown={e => draggier.onMousedown(e, {vertical: Direction.end, horizontal: Direction.center})}/>
            </>}
            {!!props.resize.width && <>
                <div class="visual-block-resize  visual-block-resize-left" onMousedown={e => draggier.onMousedown(e, {vertical: Direction.center, horizontal: Direction.start})}/>
                <div class="visual-block-resize  visual-block-resize-right" onMousedown={e => draggier.onMousedown(e, {vertical: Direction.center, horizontal: Direction.end})}/>
            </>}
            {!!props.resize.height && !!props.resize.width && <>
                <div class="visual-block-resize  visual-block-resize-top-left" onMousedown={e => draggier.onMousedown(e, {vertical: Direction.start, horizontal: Direction.start})}/>
                <div class="visual-block-resize  visual-block-resize-top-right" onMousedown={e => draggier.onMousedown(e, {vertical: Direction.start, horizontal: Direction.end})}/>
                <div class="visual-block-resize  visual-block-resize-bottom-left" onMousedown={e => draggier.onMousedown(e, {vertical: Direction.end, horizontal: Direction.start})}/>
                <div class="visual-block-resize  visual-block-resize-bottom-right" onMousedown={e => draggier.onMousedown(e, {vertical: Direction.end, horizontal: Direction.end})}/>
            </>}
        </>
    },
})