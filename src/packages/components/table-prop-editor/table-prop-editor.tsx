import {defineComponent, PropType} from 'vue';
import {ElButton, ElTag} from "element-plus";
import {VisualEditorProp} from "@/packages/editor.props";
import './table-props-editor.scss'
import {TablePropEditService} from "@/packages/components/table-prop-editor/table-prop-editor.service";

export const TablePropEditor = defineComponent({
    props: {
        modelValue: {type: Array as PropType<any[]>},
        config: {type: Object as PropType<VisualEditorProp>, required: true},
    },
    emits: {
        'update:modelValue': (val?: object[]) => true,
        'change': (val?: object[]) => true,
    },
    setup(props, ctx) {

        const openEdit = async () => {
            const newData = await TablePropEditService({
                config: props.config,
                data: props.modelValue,
            })
            ctx.emit('update:modelValue', newData)
            ctx.emit('change', newData)
        }

        return () => {
            return (
                <div class="table-props-editor" {...{onClick: openEdit} as any}>
                    {(!props.modelValue || props.modelValue.length === 0) ? (
                        <ElButton icon="el-icon-plus" size="mini">添加</ElButton>
                    ) : (props.modelValue || []).map((opt, index) => (
                        <ElTag key={index} size="small">
                            {!props.config.showField ? 'miss showField!' : opt[props.config.showField]}
                        </ElTag>
                    ))}
                </div>
            )
        }
    },
})